//Arduino EFM Code
//  Tyler Welty
//    The purpose of this code is to test saving and opening a new
//    file every 2 minutes to minimize data loss due to corruption
//    or other potential internal file errors.


//Define Variable to Note to Change Prescaler
#define ADCSPEED 1
//Define Functions for Setting and Clearing Bits
#ifndef cbi //Clear Bit Function (Set to 0)
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi //Set Bit Function (Set to 1)
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

//Include Necessary Libraries
  //For SD Card
#include <SPI.h>
#include "SdFat.h"
  //For RTC
#include <Wire.h>
#include "RTClib.h"

//Global File Setup
SdFat sd;
SdFile DataFile;
//Global RTC Setup
RTC_PCF8523 rtc;

//Global Variables
uint8_t RunProgram; //Determine whether to run program or not
uint8_t BuffSelect; //Determines which buffer is stored to, 0, 1, 2, or 3
uint32_t i; //Keeps Track of Seconds Passed in Program
uint16_t SampleCount;
  //4 512 byte Buffers to Contain Data Written to SD Card in Blocks
uint8_t BUFF0[512];
uint8_t BUFF1[512];
uint8_t BUFF2[512];
uint8_t BUFF3[512];
  //Counters to Keep Track of How Full Buffers Are
uint16_t a; //Counter for BUFF0
uint16_t b; //Counter for BUFF1
uint16_t c; //Counter for BUFF2
uint16_t d; //Counter for BUFF3
  //Time Variables
//uint8_t StartTime; //For Knowing when Main Loop Starts
//uint32_t RunTime = 120; //Time to Run Program, in Seconds
uint8_t MarkNewSecond;  //Variable to Note When a New Second Has Occured
uint32_t CurrentTime; //To Keep Track of How Many Seconds Have Passed Since the Program Started
uint32_t MissedSamples;
uint8_t SDSelect;
  //Pin Setup
//const uint8_t CSpinActual = 10; //Set Pin for Use on Chip Select for SD Card
//const uint8_t CSpinMega = 53; //Set Pin so Mega Thinks its Default CS Pin is Right for SPI Communications
  //Serial Communications Rate
//uint32_t baudRate = 115200;


//Begin Setup Loop
void setup() 
{  
//***ADC Free-run Setup Code*** 
   //Set ADC Speed to Desired Value
  //Uncomment Sections as needed, but make sure only one setting is uncommented at a time
  
  #if ADCSPEED //If ADCSPEED is true (set true earlier), then:
  //Set Prescaler to 128 ((Default Division Factor) 125 kHz/13 Clock Cycles = 9600 sps (Values are ideal))
   /*sbi (ADCSRA, ADPS2);  //Sets ADPS2 (ADPS2 = "ADC Prescaler bit 2") to 1
   sbi (ADCSRA, ADPS1);
   sbi (ADCSRA, ADPS0);*/
  
  //Set Prescaler to 64 (250 kHz/13 CC = 19200 sps)
   sbi (ADCSRA, ADPS2);
   sbi (ADCSRA, ADPS1);
   cbi (ADCSRA, ADPS0);
  
  //Set Prescaler to 32 (500 kHz/13 CC = 38500 sps)
   /*sbi (ADCSRA, ADPS2);
   cbi (ADCSRA, ADPS1);
   sbi (ADCSRA, ADPS0);*/
  
  //Set Prescaler to 16 (1 MHz/13 CC = 77000 sps)
   /*sbi (ADCSRA, ADPS2);
   cbi (ADCSRA, ADPS1);
   cbi (ADCSRA, ADPS0);*/
   
  //Set Prescaler to 8 (2 MHz/13 CC = 154000 sps)
   /*cbi (ADCSRA, ADPS2);
   sbi (ADCSRA, ADPS1);
   sbi (ADCSRA, ADPS0);*/
  #endif //End of ADC Prescaler Settings
  
  //Set Desired Reference Voltage Source for ADC
   //Set External AREF as Vref (Connected to 5V power supply)
  cbi (ADMUX, REFS1);
  sbi (ADMUX, REFS0);
  
  
  //Set ADC Multiplexer to Read Only Desired Analog Pin
    //A0
  cbi (ADCSRB, MUX5);
  cbi (ADMUX, MUX4);
  cbi (ADMUX, MUX3);
  cbi (ADMUX, MUX2);
  cbi (ADMUX, MUX1);
  cbi (ADMUX, MUX0);
  
  
  //Setup for ADC Free-Running Mode
  sbi (ADCSRA, ADEN);  //Set ADC Enable Bit to 1, Turning on the ADC
  sbi (ADCSRA, ADATE); //Set ADC Auto-Trigger Enable Bit to 1, Turning on Free-Running Mode
  sbi (ADCSRA, ADIE);  //Set ADC Interrupt Enable Bit to 1, Turning on ADC Interrupts
  cbi (ADCSRB, ADTS2); //Set ADC Timers to Free-Running Mode (ADTS Changes, All to 0)
  cbi (ADCSRB, ADTS1);
  cbi (ADCSRB, ADTS0);
    
//***SD Card Setup Code***
  
  //Make Sure Old File is Gone
  sd.remove("Test.bin");
  
  //Initialize Chip Select Pins
  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);
  pinMode(53, OUTPUT);
  digitalWrite(53, HIGH);
  
  //Start Serial Comms. for Checking SD Card
  Serial.begin(115200);
  
  while(!Serial) //Wait for Serial Communications to Begin
  {;}
  
  if (!sd.begin(10))
  {
    Serial.println (F("There was an error in connecting to the SD Card..."));
    RunProgram = 0;
  }
    else
    {
      Serial.println(F("sd.begin worked, connection to SD Card established."));
      RunProgram = 1;
    }

  if(!DataFile.open("Test.bin", O_CREAT | O_WRITE))
  {
    Serial.println(F("Could not open test file..."));
    RunProgram = 0;
  }
    else
    {
      Serial.println(F("Able to successfully open test file."));
      DataFile.close();
    }
  
  //Set SPI Communication to SD Card at Fastest Speed (4MHz)
  cbi (SPCR, SPI2X);
  cbi (SPCR, SPR1);
  cbi (SPCR, SPR0);

//***RTC Setup Code***
 
  if (!rtc.begin()) 
  {
    Serial.println(F("Couldn't find RTC"));
    RunProgram = 0;
  }
    else
    {
      Serial.println(F("Successfully connected to RTC"));
    }

  if (!rtc.initialized()) {
    Serial.println(F("RTC is NOT running! Setting time now..."));
    // Following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }

//***RTC Second Interrupt Routine Setup (Function "MarkSecond")
  pinMode(2, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(2), MarkSecond, RISING);
  rtc.writeSqwPinMode(6); //Tells RTC to send a square wave with 1Hz frequency (width is 500ms)
  
//***Initialize Test Variables***
  BuffSelect = 0; //Start with Buffer 0, will alternate with Buffer 1
  SDSelect = 0;
  i = 0;  //Set Seconds Counter to 0
  CurrentTime = 0;
  MarkNewSecond = 0;
  SampleCount = 0;
  MissedSamples = 0;
    //Set Buffer Counters to 0
  a = 0;
  b = 0;
  c = 0;
  d = 0;

  CreateHeader();
  NewFile();

  /*DateTime now = rtc.now();
  StartTime = now.second(); //Initialize StartTime to End of Setup Loop*/
  sbi (ADCSRA, ADSC); //Start First Conversion, Let Free-Running Mode Go Freeeee!
}

//Main Function that Loops after Setup in Complete
void loop() 
{
  if(RunProgram == 1)  //If Setup Was Successful, RunProgram will be 1 and allow Desired Loop to Run
  { 
    //Check to See if Program has Finished Predetermined RunTime
    /*if(i >= RunTime)  //If Predetermined Test RunTime is met, Ends Main Loop
    {
      RunProgram = 0;
      return;
    }*/
    //Check to See if 2 Minutes Have Passed and It's Time for a New File
    if(i >= CurrentTime+120)
    {
      NewFile();
      CurrentTime = i;
      a = 0;
      b = 0;
      c = 0;
      d = 0;
    }
    
    //Check if Buffers are full
    if(a >= 511 && SDSelect == 0) //If Buffer 0 is full (256 samples taken), write to DataFile
    {
      uint32_t T1 = micros();
      DataFile.write(BUFF0, 512); //Write to file
      DataFile.flush();           //Save what was just written
      uint32_t T2 = micros();
      Serial.println(T2 - T1);
      SDSelect++;
      a = 0;                      //Reset Counter for buffer0
    }
      else if(b >= 511 && SDSelect == 1) //Otherwise check Buffer 1; if full, write to DataFile
      {
        uint32_t T1 = micros();
        DataFile.write(BUFF1, 512);
        DataFile.flush();
        uint32_t T2 = micros();
        Serial.println(T2 - T1);
        SDSelect++;
        b = 0;
      }
      else if(c >= 511 && SDSelect == 2)  //If Buffer2 is selected and full, write to SD card
      {
        uint32_t T1 = micros();
        DataFile.write(BUFF2, 512);
        DataFile.flush();
        uint32_t T2 = micros();
        Serial.println(T2 - T1);
        SDSelect++;
        c = 0;
      }
      else if(d >= 511 && SDSelect == 3)  //If Buffer3 is selected and full, write to SD card
      {
        uint32_t T1 = micros();
        DataFile.write(BUFF3, 512);
        DataFile.flush();
        uint32_t T2 = micros();
        Serial.println(T2 - T1);
        SDSelect = 0;
        d = 0;
      }
  }
    else  //If RunProgram = 0, then End Communciations and Shut-Off ADC
    {
      cbi (ADCSRA, ADEN); //Turn Off ADC
      detachInterrupt(digitalPinToInterrupt(2));
      DataFile.close();
      Serial.print("Samples missed in 2 minutes: ");
      Serial.println(MissedSamples);
      Serial.println(F("Done"));
      Serial.end();
      return;
    }
}

//Interrupt Routine for ADC Completed Conversion
ISR(ADC_vect)
{ 
  //Read Values From ADC Registers
  uint8_t ADCval1 = ADCL;
  uint8_t ADCval2 = ADCH;

  if(MarkNewSecond == 1)
  {
    ADCval2 += 1<<7;
    MarkNewSecond = 0;
  }

  //If Buffer Select = 0, Store to Buffer 0
  if(BuffSelect == 0 && a< 512)
  {
    //Store Time and ADC value in Buffer as a 2 byte integer
    BUFF0[a] = ADCval2;  //Store ADCH and time bits in Buffer space
    a++; //Increase count of how many samples are in buffer
    BUFF0[a] = ADCval1;  //Store ADCL bits in next Buffer space
    a++;  
    if(a == 512)
    {
      BuffSelect = 1;
    }
    //***Note, this is big-endian format, as most significant bit in sample is stored first***
  }
    else if (BuffSelect == 1 && b < 512) //Otherwise Store in Buffer1
    {
      //Store Time and ADC value in Buffer as a 2 byte integer
      BUFF1[b] = ADCval2;  //Store ADCH and time bits in Buffer space
      b++; //Increase count of how many samples are in buffer
      BUFF1[b] = ADCval1;  //Store ADCL bits in next Buffer space
      b++;
      if(b == 512)
      {
        BuffSelect = 2;
      }
    }
    else if (BuffSelect == 2 && c < 512) //Otherwise Store in Buffer2
    {
      //Store Time and ADC value in Buffer as a 2 byte integer
      BUFF2[c] = ADCval2;  //Store ADCH and time bits in Buffer space
      c++; //Increase count of how many samples are in buffer
      BUFF2[c] = ADCval1;  //Store ADCL bits in next Buffer space
      c++;  
      if(c == 512)
      {
        BuffSelect = 3;
      }
    } 
    else if (BuffSelect == 3 && d < 512) //Otherwise Store in Buffer3
    {
      //Store Time and ADC value in Buffer as a 2 byte integer
      BUFF3[d] = ADCval2;  //Store ADCH and time bits in Buffer space
      d++; //Increase count of how many samples are in buffer
      BUFF3[d] = ADCval1;  //Store ADCL bits in next Buffer space
      d++;  
      if(d == 512)
      {
        BuffSelect = 0;
      }
    }
    else
    {
      MissedSamples++;
    }
}

void MarkSecond()
{
  MarkNewSecond = 1;
  i++;
}

void NewFile()
{
  //Disable Interrupts So Data is in the Proper File
  cbi(ADCSRA, ADIE);
  detachInterrupt(digitalPinToInterrupt(2));
  //Close Old File
  DataFile.close();
  //Read RTC to Get Time and Date, then Use that to Make a New File with Dated Name
  DateTime now = rtc.now();
  String FILENAME = "";
  int DAY = now.day();
  int HOUR = now.hour();
  int MINUTE = now.minute();
  int SECOND = now.second();
  if (DAY < 10)
  {
    FILENAME += '0';
  }
  FILENAME += DAY;
  if (HOUR < 10)
  {
    FILENAME += '0';
  }
  FILENAME += HOUR;
  if (MINUTE < 10)
  {
    FILENAME += '0';
  }
  FILENAME += MINUTE;
  if (SECOND < 10)
  {
    FILENAME += '0';
  }
  FILENAME += SECOND;
  FILENAME += ".bin";
  int StrLength = FILENAME.length() + 1;
  char FileName[StrLength];
  FILENAME.toCharArray(FileName, StrLength);
  //Open New File
  DataFile.open(FileName, O_CREAT | O_WRITE);

  delay(1);
  //Re-allow Interrupts, Resume Taking Data
  sbi(ADCSRA, ADIE);
  attachInterrupt(digitalPinToInterrupt(2), MarkSecond, RISING);
}

void CreateHeader()
{
  DateTime now = rtc.now();
  
  //Create Variable Header Filename FILENAME in .txt format based on time of header creation
  String FILENAME = "_";
  int YEAR = now.year();
  int MONTH = now.month();
  int DAY = now.day();
  int HOUR = now.hour();
  if (YEAR < 10)
  {
    FILENAME += '0';
  }
  FILENAME += YEAR;
  if (MONTH < 10)
  {
    FILENAME += '0';
  }
  FILENAME += MONTH;
  if (DAY < 10)
  {
    FILENAME += '0';
  }
  FILENAME += DAY;
  if (HOUR < 10)
  {
    FILENAME += '0';
  }
  FILENAME += HOUR;
  FILENAME += ".txt";
  int StrLength = FILENAME.length() + 1;
  char HeaderName[StrLength];
  FILENAME.toCharArray(HeaderName, StrLength);
  
  //Open Header File
  DataFile.open(HeaderName, O_CREAT | O_WRITE);
  DataFile.println("Arduino EFM Project Sensor");
  DataFile.println("Created by Tyler Welty and Dr. Phillip Bitzer");
  DataFile.println("Under funding of the University of Alabama in Huntsville");
  DataFile.println("");
  DataFile.println("Sensor Information:");
  DataFile.println("Sensor #0");
  DataFile.print("Data of Test Start: ");
  DataFile.print(now.month(), DEC);
  DataFile.print("-");
  DataFile.print(now.day(), DEC);
  DataFile.print("-");
  DataFile.println(now.year(), DEC);
  DataFile.print("Time of Test Start: ");
  DataFile.print(now.hour(), DEC);
  DataFile.print(":");
  DataFile.print(now.minute(), DEC);
  DataFile.print(":");
  DataFile.println(now.second(), DEC);

  //***Make sure now function is always current time***

  DataFile.close();  
}








