# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 16:23:40 2018

@author: T-Dawg
"""

"""
Part of the AEFM Modeling Code

    This section of code is 
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
"""
"""
    Major To-Do's:
        - 
        
    Updates:
        20 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        
        Inputs:
            
            
        Returns:
            
            
        
"""
#Import Necessary Libraries
import math as m



#Global Variables


#Define Function
def SensorDistanceFromCharge(sensor_x, sensor_y, sensor_z, ltg_x, ltg_y, ltg_z):
    #Find Difference in x- Dimension
    x_diff = ltg_x - sensor_x
    #Find Difference in y-Dimension
    y_diff = ltg_y - sensor_y
    #Find Difference in z-Dimension
    z_diff = ltg_z - sensor_z
    
    #Find Magnitude of this Distance (All that we need for delta(E) eqn)
    sensor_dist = m.sqrt((x_diff**2)+(y_diff**2)+(z_diff**2))
    
    #Return Final Distance to Main
    return sensor_dist
    
    




















