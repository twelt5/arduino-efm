# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 16:23:45 2018

@author: T-Dawg
"""

"""
Part of the AEFM Modeling Code

    This section of code is 
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
"""
"""
    Major To-Do's:
        - 
        
    Updates:
        20 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        
        Inputs:
            
            
        Returns:
            
            
        
"""
#Import Necessary Libraries
import math
#import numpy as np



#Global Variables
epsilon = 8.85e-12 #Permittivity of Free Space; F⋅m−1 (farads per meter)

#Define Function
def SensorDeltaE(sensor_dist, charge_val):
    #Calculate delta(E) based on eqn for monopole charge retrieval in Krehbiel et al. 1979
    deltaE = (2*charge_val)/(4*3.14*epsilon*(sensor_dist**3))
    
    
    #Return delta(E) value back to main for usage
    return deltaE















