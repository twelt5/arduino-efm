# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 16:10:21 2018

@author: T-Dawg
"""

"""
Arduino Electric Field-Change Meter (AEFM) 2.0 Sensor Modeling Code

    The purpose of this code is to model the sensor response of Arduino EFM
    to known lightning discharges and simulate the sensor results.
    In addition, the code will be able to simulate an entire array of Arduino
    EFM sensors to prove that the system is viable and practical in full
    scale deployment.
    
    Later on features will be added to bring in HAMMA data for direct
    comparison to a known, similar lightning sensor. These comparisons will
    further validate the results of the Arduino EFM and these models.
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
        Phillip Bitzer
            bitzerp@uah.edu
    
    Code is funded by the ESSC at The University of Alabama in Hunstville. 
    This project is a subset of the HAMMA group work.
    
"""

"""
    Major To-Do's:
        - Add in loops to test many lightning sources for an entire study area
        - Work on finishing all extrenous functions
        - Amp up graphing to a more scatterplot type thing, map sensors, etc.
        - Decide on a sensor array to test, find locations
        - Model HAMMA response for comparison?
        - Figure out efield relationships, how to model flashes and how efield changes over a flash
        - Add in statistics calcultions and values for array
        - ***Figure out how to reverse solve the delta(efields) for charge - what matrix math do I need to solve?
        
    Updates:
        22 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        MAIN - so all the functions are ran in order here as a script;
                variables are described next to or above them in "Global Variables"
          
General Code Flow of Main:
    1) Setup general parameters, arrays, libraries
    2) Generate Simulated Lightning E-Field
    3) For each sensor, iterate through functions needed to find voltage from flash
    4) For each sensor, find basic flash statistics
    5) Graph array response?
    
    Alternatively, could simulate lightning strikes in many locations;
    To do this, would need to repeat above process for different locations
    1) Generate lightning location
    2) From above step 2, keep e-field same
    3) For each location of lightning, repeat step 3/4 above
    4) Graph Results
"""


#Import Necessary Libraries
import numpy as np
from matplotlib import pyplot as plt

#Import Homemade Functions
from Source_Charge import SourceCharge
from Sensor_Distance_from_Source import SensorDistanceFromCharge
from Sensor_Delta_Efield import SensorDeltaE
from Charge_Retrieval_from_Sensors import SensorRetrievedCharge
from Charge_Retrieval_Plotting import ChargeContour


#Global Variables
#   Code Parameters
ltg_type = 0 #0 = testing mode(...what charge to use?), 1 = simple CG, 2 = simple IC?
#   Graph Parameter
graph_vs_time = 0 #0 = will graph versus sample number; 1 = will graph per unit time
#   Global Sensor Parameters
sample_rate = 19200 #Number of samples in a second/in a single flash event; determines array size
#   Dimensions to Test Lightning Charge Over
ltg_x_dim = 10
ltg_y_dim = 10
ltg_altitude = 5

#   Sensor Locations (x,y,z referenced to an origin at center of array; max dimensions of study area: ? units: km;
#       NOTE: uppermost left part of ltg array is origin; base all sensor locations off of this
sensor1_loc = (0.0, 0.0, 0.0) #Located at origin center of study area (in km)
sensor2_loc = (5.0, 0.0, 0.0)
sensor3_loc = (5.0, 5.0, 0.0)
sensor4_loc = (0.0, 5.0, 0.0)
sensor5_loc = (-5.0, 5.0, 0.0)
sensor6_loc = (-5.0, 0.0, 0.0)
sensor7_loc = (-5.0, -5.0, 0.0)
sensor8_loc = (0.0, -5.0, 0.0)
sensor9_loc = (5.0, -5.0, 0.0)

#       ADD: either generate sensor array locations automatically via fnct or create manual array

#   Sensor Location List
sensor_locs = [sensor1_loc, sensor2_loc, sensor3_loc, sensor4_loc, sensor5_loc, sensor6_loc, sensor7_loc, sensor8_loc, sensor9_loc]
#   Quick Number of Sensors
num_sensors = len(sensor_locs)
#   Create Empty Distance Array the size of the number of sensors to store sensor to source distances for delta(E)
dist_arr = np.zeros(num_sensors)
#   Create Empty Delta(E) Array to hold the calculated delta(E) for each sensor
deltaE = np.zeros(num_sensors)
#Create Empty Array Same Size as ltg_charge_arr to hold all of the sensor charge values
sensor_charge_arr = np.zeros((ltg_y_dim, ltg_x_dim))

#   Lightning Location
#ltg_charge_loc = (2.5, 2.5, 5.0)   #lightning location from origin, in km


#Generate Charge Value for Chosen Type of Lightning Flash
charge_val = SourceCharge(ltg_type)

#Generate "Grid" of Points Given Certain Dimensions 
#   Dimensions should be larger than those of the array and given in global variables
#   Generate numpy array with nxm dimensions;
#       fill each data point with the given charge value determined by lightning type
#   NOTE: Uppermost left value (0,0) of ltg array is the origin for the grid. 
#           All other things need to be graphed relative to this;
#           ALSO -> each grid point for ltg counts as 1 unit of distance; ie, ltg_charge_arr[5,3] is 5 units distance in the y direction, 3 in the x-direction
ltg_charge_arr = np.full((ltg_y_dim, ltg_x_dim), charge_val)


#Now for Each Point in the Array:
#   Find delta(E) of each sensor
#   Use delta(E)'s to find sensor charge
#   Compare given sensor charge and known ltg charge

#For Loop for Each Grid Point:
    #Note: Numpy arrays follow row,column format, so...
#   For Each Row in ltg_charge_arr
for y in range(ltg_y_dim):
    #For Each column in ltg_charge_arr
    for x in range(ltg_x_dim):
        #Now iterating through for each grid point with a lightning charge
        
        #Iterate next step(s) for each sensor:
        for t in range (num_sensors):
            #In order to find delta(E) from charge at sensor, need:
            #    Distance from the sensor to the ltg charge source
                
            #Find Distance between Lightning Source and given Sensor
            #   Pass sensor location and lightning location ((x, y) here) to distance fnct
            #   Store in an array with distance for each sensor
            dist_arr[t] = SensorDistanceFromCharge(sensor_locs[t][0], sensor_locs[t][1], sensor_locs[t][2], x, y, ltg_altitude)
                            
            #Generate delta(E-Field) at the Sensor Plates
            #   ***Need new file and function for this
            deltaE[t] = SensorDeltaE(dist_arr[t], ltg_charge_arr[y][x])
               
            #END SENSOR LOOP.............................................................................................................
                
        #Find charge generated by sensor array
        #   Send all needed variable to function that generates q value from given delta(E)'s
        #   Then store generated q value in a new array (same dimensions as ltg) -> will compare later on
        sensor_charge_arr[y][x] = SensorRetrievedCharge(deltaE)
        
        
        
        
        
    #END COLUMN LOOP.....................................................................................................................
#END ROW LOOP...........................................................................................................................
        
        
        
        
#Generate stats for each sensor, output somewhere
#Figure out how to pass stats values for each sensor back and forth; lists within a list?
#   WILL USE LIST WITHIN A LIST (hopefully doesn't get confusing...)
#   ***Need new file and function for this




#Using found delta(efields), estimate charge using the sensor readings
#   ***Need new file and function for this





#Store Plot
# Create a 2d contour plot with 3 subplots containing sensor retrieved charge
#   eventually also want average delta(E) and average distance from sensors as subplots so all
#       3 are on same plot and easy to compare/see
#ChargeContour(sensor_charge_arr, sensor_locs)




#Calculate General Stats for All Sensors Based on Stats Calculated per Sensor
#   ***Need new file and function for this



#Compare Results Quantitatively to a Comparable HAMMA Array
#   ***Need new file and function for this










