# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 18:39:33 2018

@author: Tyler Welty
"""

"""
Part of the AEFM Modeling Code

    This code will take the charge on the plate and, using a linear
    relationship between charge and sensor response, calculte sensor
    voltage. To more accurately simulate sensor response, the values will
    also be limited to the more imprecise resolution of the sensor 
    compared to the full floating point value (5mV versus the uV float gives,
    which is unrealistic).
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
"""
"""
    Major To-Do's:
        - Fix adjusting to resolution code
        
    Updates:
        20 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        
        Inputs:
            volt_arr: takes in sensor voltage reference so it can fill in the array with sensor response values
            charge_arr: takes in charge array reference so it can use these values to generate voltage values
            
        Returns:
           Nothing; the volt_arr from the main function should now be filled with values instead of 0's, however.
            
        
"""
#Import Necessary Libraries
import numpy as np


#Global Variables
#   Constants for Voltage Conversion
A = 1   #CHANGE VALUE - find an actual reasonable value for these
B = 0
#   Sensor Resolution
volt_resolution = 0.005  #equivalent to 5 mv resolution of sensor

#Define function:
def aefm_voltage(volt_arr, charge_arr):
    for x in np.nditer(volt_arr, op_flags=['readwrite']):
        #Generate overly precise voltage sensor should read
        #   In Bitzer's Dissertation, it is noted that sensors like AEFM (Marx Meters)
        #   Have a linear relationship between signal response and charge:
        #       q = AV+B        , where A and B are constants. After rearranging,
        #   get equation for voltage is:
        #       V = (q-B)/A
        #   so we fill out the volt_arr as follows:
        volt_arr[:] = (charge_arr[:]-B)/A


    
    #NOTE: AEFM can only measure in a certain voltage resolution.
    #       To simulate this resolution, will round to nearest 
    #       resolution to get a more accurate simulated reading.
    """
    WIP

    #       Start by finding total number of resolutions within measurement.
    #       Then, multiply number of resolutions by the resolution.
    #       Subtract this multiplied value from the orgininal measurement.
    #       See if need to round up or down, then round accordingly.
    #       Set new rounded value as result in aef_volt
    #   Create array for number of resolutions
    num_resolutions = np.zeros(sample_rate, dtype=int)
    #   Create array for remainder checks
    remain_arr = np.zeros(sample_rate, dtype=float)
    #   creater counter array?
    """
    """num_resolutions[:] = (aefm_volt[:]/0.005)
    for t in num_resolutions[:]:
        num_resolutions[t] = int(num_resolutions[t])
    for t in np.nditer(remain_arr, op_flags=['readwrite']):
        remain_arr[:] = aefm_volt[:]-num_resolutions[:]*0.005
        remain_val = remain_arr[t]
        if remain_val < 0.0025:
            aefm_volt[t] = num_resolutions[t]*0.005
        else:
            aefm_volt[t] = num_resolutions[t]*0.005+0.005
            """

    """for t in np.nditer(aefm_volt, op_flags=['readwrite']):
        num_resolutions[:] = (aefm_volt[:]/0.005)
        num_resolutions[t] = int(num_resolutions[t])
        remain_arr[:] = (aefm_volt[:]-(num_resolutions[t]*0.005))
        if remain_arr[t] < 0.0025:
            aefm_volt[t] = (num_resolutions[t]*0.005)
        else:
            aefm_volt[t] = (num_resolutions[t]*0.005)+0.005
            """      



