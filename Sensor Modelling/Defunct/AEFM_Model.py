# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 21:52:59 2018

@author: Tyler Welty
"""

"""
Arduino Electric Field-Change Meter (AEFM) 2.0 Sensor Modeling Code

    The purpose of this code is to model the sensor response of Arduino EFM
    to known lightning discharges and simulate the sensor results.
    In addition, the code will be able to simulate an entire array of Arduino
    EFM sensors to prove that the system is viable and practical in full
    scale deployment.
    
    Later on features will be added to bring in HAMMA data for direct
    comparison to a known, similar lightning sensor. These comparisons will
    further validate the results of the Arduino EFM and these models.
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
        Phillip Bitzer
            bitzerp@uah.edu
    
    Code is funded by the ESSC at The University of Alabama in Hunstville. 
    This project is a subset of the HAMMA group work.
    
"""

"""
    Major To-Do's:
        - Change test values -> find common e-field values for testing
        - Add in loops to test many lightning sources for an entire study area
        - Work on finishing all extrenous functions
        - Amp up graphing to a more scatterplot type thing, map sensors, etc.
        - Decide on a sensor array to test, find locations
        - Model HAMMA response for comparison?
        - Figure out efield relationships, how to model flashes and how efield changes over a flash
        - Add in statistics calcultions and values for array
        - Should I switch to 2D simulation? Is 3D necessary? (minimal code change - eliminate z component in loc values here and in Sensor_Distance_From_Source fnct)
        
    Updates:
        17 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        MAIN - so all the functions are ran in order here as a script;
                variables are described next to or above them in "Global Variables"
          
General Code Flow of Main:
    1) Setup general parameters, arrays, libraries
    2) Generate Simulated Lightning E-Field
    3) For each sensor, iterate through functions needed to find voltage from flash
    4) For each sensor, find basic flash statistics
    5) Graph array response?
    
    Alternatively, could simulate lightning strikes in many locations;
    To do this, would need to repeat above process for different locations
    1) Generate lightning location
    2) From above step 2, keep e-field same
    3) For each location of lightning, repeat step 3/4 above
    4) Graph Results
"""


#Import Necessary Libraries
import numpy as np
#import matplotlib

from AEFM_Voltage import aefm_voltage 
from Array_Response_Plots import volt_graph
from Ltg_Efield_Generation import ltg_efield_generation
from Plate_Charge import charge_on_plate
from Plate_Efield_Conversion import plate_efield
from Sensor_Distance_From_Source import sensor_distances
#from Sensor_Gains import
#from Sensor_Simulation_Stats import

#Global Variables
#   Code Parameters
ltg_type = 0 #0 = testing mode, 1 = simple CG, 2 = simple IC
#   Graph Parameter
graph_vs_time = 0 #0 = will graph versus sample number; 1 = will graph per unit time
#   Global Sensor Parameters
sample_rate = 19200 #Number of samples in a second/in a single flash event; determines array size
#   Sensor Locations (x,y,z referenced to an origin at center of array; max dimensions of study area: 40kmx40kmx12km; units: km;
sensor1_loc = (0.0, 0.0, 0.0) #Located at origin center of study area (in km)
sensor2_loc = (2.5, 0.0, 0.0)
sensor3_loc = (5.0, 0.0, 0.0)

#       ADD: either generate sensor array locations automatically via fnct or create manual array

#   Sensor Location List
sensor_locs = [sensor1_loc, sensor2_loc, sensor3_loc]
#   Quick Number of Sensors
num_sensors = len(sensor_locs)
#   Lightning Location
ltg_loc = (5.0, 0.0, 0.0)   #lightning location from origin, in km

#   Setup Arrays for Iterating with Time
#       Create empty array for electric field at source of discharge
e_source = np.zeros(sample_rate, dtype=float) #units:
#       Create empty array for electric field going through the sensing electrode plate
e_plate = np.zeros(sample_rate, dtype=float) #units:
#       Create empty array for charge on sensing electrode plate due to electric field
plate_charge = np.zeros(sample_rate, dtype=float) #units:
#       Create empty array for simulated voltage read by sensor
aefm_volt = np.zeros(sample_rate, dtype=float) #units:


#Populate E-Field Source Array with E-Field from a Simulated Flash
ltg_efield_generation(e_source, sample_rate, ltg_type)

#ADD - incorporate way to generate multiple ltg locations; for now, just added as a parameter under global variables


#Find Distance between Lightning Source and Sensors
sensor_dist = sensor_distances(ltg_loc, sensor_locs)

#Iterate next steps for each sensor:
for t in range (num_sensors):
    #Generate E-Field at the Sensor Plates after Propagation
    plate_efield(e_plate, sensor_dist, e_source, t)

    #Find the Sensor Gains?    
    #   INSERT GAIN FUNCTION HERE
    #for now, assume following value:
    gain = 1.0

    #Find Charge on Plates from E-Field through the Plate
    charge_on_plate(plate_charge, e_plate, gain)

    #Relate Charges to Voltages to Simulate Sensor Responses
    aefm_voltage(aefm_volt, plate_charge)

    #Plot AEFM Results
    volt_graph(aefm_volt, t, graph_vs_time)
    
    #Generate stats for each sensor, output somewhere









