# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 17:19:59 2018

@author: T-Dawg
"""

"""
Write function here for generating lightning e-field

REQUIRES: e_source to get passed to it, sample_rate to get passed, and ltg_type

"""
#Import Necessary Libraries
import numpy as np



def ltg_efield_generation(e_source, sample_rate, ltg_type):
    
    #   Generate Test Values for e_source if type=0
    if type == 1:
        #Populate e_source with some standard test values within the range of e-field for lightning
        #   Setup Counters for each 1/4 of e_source array
        i1 = 0
        i2 = int((sample_rate/4))
        i3 = int((sample_rate/4)*2)
        i4 = int((sample_rate/4)*3)
        #   Start with some 0's
        #       Make about first 1/4 values 0
        for i in np.nditer(e_source, op_flags=['readwrite']):
            e_source[i1:i2] = 0.0 
        #   Fill out next 1/4 with high e-field values
        for i in np.nditer(e_source, op_flags=['readwrite']):
            e_source[i2:i3] = 5.0 ###CHANGE VALUE
        #   Fill out next 1/4 with low e-field values
        for i in np.nditer(e_source, op_flags=['readwrite']):
            e_source[i3:i4] = -5.0 ###CHANGE VALUE
        #   Fill out end with a sine wave pattern?
        for i in np.nditer(e_source, op_flags=['readwrite']):
            e_source[i4:] = 0.0 ###CHANGE VALUE
        #
        return e_source
    
    #   Generate a simple CG flash
    elif ltg_type == 1:
        print('cg')
        
    #   Generate a simle IC flash
    elif ltg_type == 2:
        print('IC')
    
    #   If no valid option given, inform user
    else:
        print()

    
