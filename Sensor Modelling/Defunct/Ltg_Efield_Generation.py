# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 17:19:59 2018

@author: Tyler Welty
"""

"""
Part of the AEFM Modeling Code

    The purpose of this section of code is to fill out the e_source array with
    e-field values for the desired type of simulated lightning flash.
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
"""
"""
    Major To-Do's:
        - Add in simulated CG and IC flashes
        - Research realistic e-field values at lightning source
        - Alter test values to be more realistic
        - Alter end from 0's -> sin wave?
        
    Updates:
        19 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        
        Inputs:
            e_source: empty numpy array passed from main function meant to be filled with e-field values of desired lightning strike
            sample_rate: sample rate of sensors, or how many samples are being simulated for (also length of e_source)
            ltg_type: what type of lightning you want to simulate; 0 = test values; 1 = simple CG; 2 = simple IC;
            
        Returns:
            Nothing directly, but e_source in the main function will now have values instead of 0's

"""

#Import Necessary Libraries
import numpy as np



def ltg_efield_generation(e_source, sample_rate, ltg_type):
    
    #   Generate Test Values for e_source if type=0
    if ltg_type == 0:
        #Populate e_source with some standard test values within the range of e-field for lightning
        #   Setup Counters for each 1/4 of e_source array
        i1 = 0
        i2 = int((sample_rate/4))
        i3 = int((sample_rate/4)*2)
        i4 = int((sample_rate/4)*3)
        #   Start with some 0's
        #       Make about first 1/4 values 0
        for i in np.nditer(e_source, op_flags=['readwrite']):
            e_source[i1:i2] = 0.0 
        #   Fill out next 1/4 with high e-field values
        for i in np.nditer(e_source, op_flags=['readwrite']):
            e_source[i2:i3] = 5.0 ###CHANGE VALUE?
        #   Fill out next 1/4 with low e-field values
        for i in np.nditer(e_source, op_flags=['readwrite']):
            e_source[i3:i4] = -5.0 ###CHANGE VALUE to something more accurate
        #   Fill out end with a sine wave pattern?
        for i in np.nditer(e_source, op_flags=['readwrite']):
            e_source[i4:] = 0.0 ###CHANGE VALUE
        #
        #return e_source
    
    #   Generate a simple CG flash
    elif ltg_type == 1:
        print('cg')
        
    #   Generate a simle IC flash
    elif ltg_type == 2:
        print('IC')
    
    #   If no valid option given, inform user
    else:
        print()

    
