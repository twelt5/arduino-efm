# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 18:37:49 2018

@author: Tyler Welty
"""

"""
Part of the AEFM Modeling Code

    This section of code is 
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
"""
"""
    Major To-Do's:
        - Add in more realistic relationship between e-field at the plate and
            e-field at the source
        
    Updates:
        20 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        
        Inputs:
            efield_plate: empty numpy array, to be filled with electric field values on the plate due to the lightning source
            efield_source: numpy array of efield values from lightning source; will be needed to convert to efield at plate
            sensor_distances: list containing the distance from the given sensor to the lightning source
            sensor_number: which sensor being ran, so that can use proper distance from sensor_distances list
            
        Returns:
            Nothing; instead, the efield_plate array referenced from the main function should now be filled with values for efield at the plate from the source
            
        
"""
#Import Necessary Libraries
import numpy as np



#Global Variables
fraction_decrease_perkm = 0.1 #% decrease in e-field per km; Currently using a test value that says the e-field linearly decreases at whatever this rate is;
                              # Assuming a linear relationship between e-field source and e-field plate for now


#Define Function
def plate_efield(efield_plate, sensor_distances, efield_source, sensor_number):
    #   Relate E-Field at Source to E-Field at Sensors
    #       For test values, assume a linear decrease with distance from source
    #           For now, just assume % decrease per km ###CHANGE TO MORE REALISTIC RELATIONSHIP SOON
    for f in np.nditer(efield_plate, op_flags=['readwrite']):
        efield_plate[:] = efield_source[:]*(1 - (sensor_distances[sensor_number])*(fraction_decrease_perkm))








