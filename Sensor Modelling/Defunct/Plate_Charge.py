# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 18:39:33 2018

@author: Tyler Welty
"""

"""
Part of the AEFM Modeling Code

    This section of code is 
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
"""
"""
    Major To-Do's:
        - Double check equation between e-field on plate and charge
        - Find true value of epsilon
        - Make r_electrode in the main function? Which variables should be in main for easy access?
        - Double check units involed and make sure there are no necessary conversion factors left out
        
    Updates:
        20 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        
        Inputs:
            gain: gain of the sensor found in previous function
            plate_charge: empty numpy array to be filled with values of charge using the e-field thru the plate
            efield_plate: numpy array containing values of the electric field; used to convert e-field values to charge
            
        Returns:
            
            
        
"""
#Import Necessary Libraries
import numpy as np

#Global Variables
r_electrode = 0.15 #Radius of sensing electrode - 15.54 cm, but in meters here
epsilon = 1 ###CHECK VALUE - not entirely sure what it's supposed to be, so assume 1?

#Define Function
def charge_on_plate(plate_charge, efield_plate, gain):
    #   Equation from Dissertation Relating Charge to E-Field (Gauss's Law)
    for c in np.nditer(plate_charge, op_flags=['readwrite']):
        plate_charge[:] = efield_plate[:]*epsilon*(3.141)*gain*(r_electrode**2)









