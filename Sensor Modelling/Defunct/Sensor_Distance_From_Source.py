# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 18:37:48 2018

@author: T-Dawg
"""

"""
Part of the AEFM Modeling Code

    The function defined here is meant to calculate the distance between a 
    given lightning source and a given array of sensors. The locations
    of the source and all sensors are the inputs. The function will return a
    list containing all the sensors' distances from the source.
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
"""
"""
    Major To-Do's:
        - 
        
    Updates:
        19 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        
        Inputs:
            ltg_loc: 3 number list containing x,y,z location of lightning source
            sensor_locs: list containing x,y,z of all of the sensors in the array
            
        Returns:
            sensor_dist: list of the distances from the sensors to the source
        
"""

#Import Necessary Libraries
import math as m


#Define Function
def sensor_distances(ltg_loc, sensor_locs):
    
    #Find number of sensors
    num_sensors = len(sensor_locs)
    
    #Create empty list for sensor differences
    sensor_dist = []
    
    #For each sensor, calculate distance from lightning source
    #   using simple vector distance math, the square root of the
    #   the squares of the differences in x, y, and z
    
    for t in range(num_sensors):
        #Note: here, t is sensor number
        
        #Find difference in x, y, and z for sensor
        diff_x = ltg_loc[0] - sensor_locs[t][0]
        
        diff_y = ltg_loc[1] - sensor_locs[t][1]
        
        diff_z = ltg_loc[2] - sensor_locs[t][2]
        
        #Calculate distance to source for sensor
        dist = m.sqrt((diff_x)**2 + (diff_y)**2 + (diff_z)**2)
        
        #Append distance to sensor_dist list
        sensor_dist.append(dist)
        
    #Return sensor distances to main funciton
    return sensor_dist












