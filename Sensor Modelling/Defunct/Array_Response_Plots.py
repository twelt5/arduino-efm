# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 18:39:44 2018

@author: Tyler Welty
"""

"""
Part of the AEFM Modeling Code

    This section of code is currently meant to plot the voltage response of
    each sensor over time/samples. Other graphing capabilities and functions
    may be added later.
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
"""
"""
    Major To-Do's:
        - Add ability to alter tick spacing
        - Add in code to make each sensor a different color chart
        - Return plots to user in some meaningful way
        
    Updates:
        19 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        
        Inputs:
            volt_array: array containing voltage values needed
            sensor_num: which sensor it is to create the corresponding and seperate plot
            time_enable: if 0, graph versus sample number; if 1, find time and graph voltage versus time (or assume any other value means plot vs time)
            
        Returns:
            Nothing at the moment; plots charts to user interface in order of sensor
            
        
"""
#Import Necessary Libraries
#import matplotlib
from matplotlib import pyplot as plt
#from matplotlib import figure
import numpy as np

#Global  Variables
figsize_x = 6 #Horizontal Chart Size in inches
figsize_y = 2 #Vertical Chart Size in inches
scale_factor = 0.1 #What fraction more than the max x and y values you want to plot; 0.1 for example makes y 10% above max value, etc.
tick_fractionx = 0.1 #What fraction of total range you want ticks to be; for versus time, will be what fraction of a second each tick is; 
                #                                                   for versus sample number, will be what percentage of samples you want each tick to be
tick_fractiony = 0.5 #Same as previous variable, but for voltage ticks
chart_color = 'black' #what color you want the voltage line to be
#ADD: CHART A DIFFERENT COLOR FOR EACH SENSOR
grid_enable = 1 #0 = no_grid; 1 = grid on
grid_color = 'gray'
grid_linestyle = ':'

def volt_graph(volt_arr, sensor_num, time_enable):
    #Create figure space
    plt.figure(sensor_num, figsize=(figsize_x, figsize_y))
    #       figsize determines size in inches (x, y)
    
    #Plot Sensor Voltage
    #   Find number of samples
    num_samples = len(volt_arr)
    #   If set to graph versus sample number, just plot:
    if time_enable == 0:
        plot = plt.plot(volt_arr)
    else:
        #Find time per sample
        #   Time per sample is equivalent to 1 s divided by sample rate
        sample_time = 1/num_samples #Units: seconds (s)
        #   Create array of times per sample
        times = np.zeros(num_samples, dtype='float')
        for x in range (1,num_samples):
            times[x] = times[x-1]+sample_time
        #   Plot voltage versus time
        plot = plt.plot(times, volt_arr)
    
    #Apply figure parameters and additions
    #   Line Options
    plt.setp(plot, color=chart_color) #Line Color
    #   Axis Labels an Title
    if time_enable == 0:
        plt.xlabel('Sample Number')
    else:
        plt.xlabel('Time (s)')
    plt.ylabel('Voltage (V)')
    title_text = 'Sensor ' + str(sensor_num+1) +' Voltage'
    plt.title(title_text)
    #   Axis Spacing and Formatting
    #       Find max and min values of voltage
    min_val = min(volt_arr)
    max_val = max(volt_arr)
    #       Make limits of y-axis min/max +/-10% max
    max_ylimit = max_val + abs(max_val*scale_factor)
    min_ylimit = min_val - abs(min_val*scale_factor)
    #       Find x-axis maximum given what we're plotting
    if time_enable == 0:
        max_xlimit = num_samples+(num_samples*scale_factor)
    else:
        max_xlimit = times[num_samples]+scale_factor
    #       Apply found axis distances
    plt.xlim(0, max_xlimit)
    plt.ylim(min_ylimit, max_ylimit)
    #   Change Chart Ticks
    #       Determine step based on tick_fraction given above
    """if time_enable == 0:
        tick_stepx = num_samples*tick_fractionx
    else:
        tick_stepx = times[num_samples]*tick_fractionx
    plt.xticks(0, max_xlimit, step=tick_stepx)
    tick_stepy = (max(volt_arr))*tick_fractiony
    plt.yticks(min_ylimit, max_ylimit, step=tick_stepy)"""
    #   Add dashed grid if desired
    if grid_enable == 1:
        plt.grid(b=True, axis='both', color=grid_color, linestyle=grid_linestyle)
        
    #Show plot to user
    plt.show()
    
    
    
    #FUTURE: Save plot as a numbered figure based on sensor_num for the user?








