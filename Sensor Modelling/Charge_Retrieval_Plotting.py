# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 16:24:09 2018

@author: T-Dawg
"""

"""
Part of the AEFM Modeling Code

    This section of code is 
    
    If you have any questions or comments, please contact:
        Tyler Welty
            tjw0024@uah.edu
            tylerwelty43@gmail.com
"""
"""
    Major To-Do's:
        - 
        
    Updates:
        20 September - ## September, 2018 : Base code written
        
"""
"""
    Function Documentation:
        
        Inputs:
            
            
        Returns:
            
            
        
"""
#Import Necessary Libraries
from matplotlib import pyplot as plt
import numpy as np


#Global Variables
#   Chart Variables





#Define Function
def ChargeContour(sensor_charge_arr, sensor_locs):
    #First, Setup Plot/Figure Area
    
    
    #Plot Sensor Retrieved Charge
    #   Plot Filled in Contour of Sensor Retrieved Charge    
    plot = plt.contourf(sensor_charge_arr)
    #    Plot contour lines for sensor retrieved charge
    
    #   Plot 2d sensor location as a scatterplot on chart
    



























