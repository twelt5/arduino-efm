# Arduino EFM Post-Processing Code
#   Made by Tyler Welty Summer 2017 under Dr. Phillip Bitzer
#       Funded by the University of Alabama in Huntsville

"""The purpose of this code is to process data from the Arduino EFM project sensor
and manipulate the data in user-friendly ways. In addition to data manipulation,
this code also contains functions to perform other necessary digital actions relating
to the sensor, such as pre-writing data files and clearing memory.

Created on Wed Jul 5 13:05:40 2017, Last Updated on """

#Necessary Header Files
import sys
import struct
import time
import os

from matplotlib import pyplot as plt
#import scipy.signal as signal
from scipy.signal import butter, lfilter, freqz, iirfilter

#Old ProcessData; returns ADCvalue and TimeValue
"""def ProcessData(fBinPath):
    StartTime = time.time() #For Recording How Long Function Takes
    fBin = open(fBinPath, 'rb')  #Open User Specified Binary File in Read in Binary Mode
    data = fBin.read()  #Read in Whole User Specified File
    fBin.seek(0)    #Go Back to Start of File
    i = int(len(data)/2)    #Measure How Many Samples are in Data Set (number of bytes/2) ***Change to 3 or 4 when needed
    i = int(len(data)/3)
    i = int(len(data)/4)
    fBin.seek(0)
    #Initialize Lists
    IntTupleList = []   #List to Store Samples Directly from Binary File
    ADCvalue = []   #List for ADC Values
    TimeValue = []  #List for Time Values
    SecondsPassed = 0
    #Read in 2 Byte Samples from IntTupleList and Store in More Useable IntList
    for sample in range(i):
        IntTupleList.append(struct.unpack(">H", fBin.read(2)))  #Reading in and Storing 2 Byte Samples
        #***For Reading in 3 byte samples, need to make sure to not store blank values, otherwise all data will be thrown off
        X = struct.unpack(">H", fBin.read(3))
        if (X <= 255):
            position = fBin.tell()
            fBin.seek(position-1)
        else:
            IntTupleList.append(X)
        #***For Reading in 4 Bytes,similar to 2 bytes
        IntTupleList.append(struct.unpack(">H", fBin.read(4)))
    IntList = [x[0] for x in IntTupleList]  #Converting IntTupleList (a list of tuples) into IntList (a list of ints)
    #Convert IntList Values into ADC Values in ADCvalue list and Time Difference Between Samples in TimeValue list
    for sample in range(i):
        ADCvalue.append(IntList[sample]&1023)   #ADC Value; remains unchanged regardless of what the time bit situation is
        TimeValue.append((IntList[sample]>>10)*2)   #Standard Time Value Read in
        #***For Reading in more than 2 bytes, use following code block instead of 2 lines above
        #ADCvalue.append(IntList[sample]&1023)
        #3byte sample, no bit math - ***Make Sure to read in 3 bytes above***
        #if value is marked as new second
        if ((IntList[sample]>>16) >= 128):
            SecondsPassed += 1
            #Mark value as special, to be highlighted in the graph or something later
            TimeValue.append(SecondsPassed*1000000)
            #Maybe alter its value later on or something?
            #Then Convert to normal value that's in the range of others by eliminating the marking and appending to list
            #X = (IntList[sample]>>16)&127
            #TimeValue.append(X)
        else:
            TimeValue.append((IntList[sample]>>16)*4)
        #3byte sample, with bit math - Make sure read 3 bytes above
        if((IntList[sample]>>10) >= 8192):
            SecondsPassed += 1
            #Mark value as special, to be highlighted in the graph or something later
            TimeValue.append(SecondsPassed*1000000)
            #Maybe alter its value later on or something?
            #Then Convert to normal value that's in the range of others by eliminating the marking and appending to list
            #X = (IntList[sample]>>10)&8191
            #TimeValue.append(X)
        else:
            TimeValue.append((IntList[sample]>>10)*4)
        #4byte sample, no bit math - make sure to read in 4 bytes above
        if((IntList[sample]>>16) >= 32768):
            SecondsPassed += 1
            #Mark value as special, to be highlighted in the graph or something later
            TimeValue.append(SecondsPassed*1000000)
            #Then Convert to normal value that's in the range of others by eliminating the marking and appending to list
            #X = (IntList[sample]>>16)&32767
            #TimeValue.append(X)                    
        else:
            TimeValue.append((IntList[sample]>>16)*4)
        #4byte sample, with bit math - make sure to read in 4 bytes above; remember, is time since last second
        if((IntList[sample]>>10) >= 2097512):
            SecondsPassed += 1
            #Mark value as special, to be highlighted in the graph or something later
            TimeValue.append(SecondsPassed*1000000)
            #Maybe alter its value later on or something?
            #Then Convert to normal value that's in the range of others by eliminating the marking and appending to list
            #X = (IntList[sample]>>10)&2097151
            #TimeValue.append(X)
        else:
            TimeValue.append((IntList[sample]>>10)*4)
    #Close Files and Wrap Things Up Nice and Tidy
    fBin.close()
    #fDec.close()
    EndTime = time.time()   #End Time of Function
    #End Message
    print ("\nData Successfully Processed. Function took ", EndTime-StartTime, "seconds to complete...\n\n\n")
    return ADCvalue, TimeValue"""

def ProcessData(fBinPath, Gain):
    StartTime = time.time() #For Recording How Long Function Takes
    fBin = open(fBinPath, 'rb')  #Open User Specified Binary File in Read in Binary Mode
    data = fBin.read()  #Read in Whole User Specified File
    fBin.seek(0)    #Go Back to Start of File
    i = int(len(data)/2)    #Measure How Many Samples are in Data Set (number of bytes/2) ***Change to 3 or 4 when needed
    """i = int(len(data)/4)"""
    fBin.seek(0)
    #Initialize Lists
    IntTupleList = []   #List to Store Samples Directly from Binary File
    VoltValue = []   #List for ADC Values
    TimeValue = []
    #Read in 2 Byte Samples from IntTupleList and Store in More Useable IntList
    for sample in range(i):
        IntTupleList.append(struct.unpack(">H", fBin.read(2)))  #Reading in and Storing 2 Byte Samples
        #***For Reading in 4 Bytes,similar to 2 bytes
        """IntTupleList.append(struct.unpack(">H", fBin.read(4)))"""
    IntList = [x[0] for x in IntTupleList]  #Converting IntTupleList (a list of tuples) into IntList (a list of ints)
    PrevT = 0
    SampleCount = 0
    #Convert IntList Values into ADC Values in ADCvalue list and Time Difference Between Samples in TimeValue list
    for sample in range(i):
        if(IntList[sample]>1023):
            if(SampleCount == 0):   #bandaid -> fix at some point
                SampleCount = 1
            AvgTSample = 1000000/SampleCount   
            for samp in range(SampleCount):
                TimeValue.append(int(AvgTSample+PrevT))
                PrevT += AvgTSample
            SampleCount = 0
        elif(sample == i-1):
            SampleCount+=1
            AvgTSample = 1000000/SampleCount   
            for samp in range(SampleCount):
                TimeValue.append(int(AvgTSample+PrevT))
                PrevT += AvgTSample
            SampleCount = 0
        VoltValue.append(((((IntList[sample]&1023)/1024)*5)-2.5)*(1/Gain))
        SampleCount += 1
    #Close Files and Wrap Things Up Nice and Tidy
    fBin.close()
    #fDec.close()
    EndTime = time.time()   #End Time of Function
    #End Message
    print ("\nData Successfully Processed. Function took ", EndTime-StartTime, "seconds to complete...\n\n\n")
    return VoltValue, TimeValue, IntList


def GraphData(TimeValue, VoltageValue):
    plt.plot(TimeValue, VoltageValue)
    #For Data With Mark New Seconds Information in them
    """TimePassed = []
    i = int(len(TimeValue))
    TimeSum = 0
    for sample in range(i):
        if (TimeValue[sample]%1000000) == 0:
            TimePassed.append(TimeValue[sample])
            TimeSum = TimeValue[sample]
        else:
            TimePassed.append(TimeValue[sample]+TimeSum)
    plt.plot(ADCvalue, TimePassed)
    return TimePassed"""
    #Use to sum and note when new seconds occur; NEEDS WORK***

def PrewriteSD(fSDwritePath, FileLength):
    #File and Function Setup
    StartTime = time.time() #Start Time of Function Action to Record Function Length
    fSDwrite = open(fSDwritePath, 'wb') #Open New File in User Prompted Place
    fSDwrite.seek(0) #Make Sure at Top of File
    BlankData = bytes(1)   #Initialize a Variable to 1 Byte of All Zero
    for sample in range(int(FileLength)):    #Write Bytes of Zero To Fill User Specified File Lenght
        fSDwrite.write(BlankData)
    fSDwrite.close()    #Close File
    EndTime = time.time()   #Measure End Time of Function for Determining Length
    #End Message
    print ("\nBlank File Ready for SD Use. Function took ", EndTime-StartTime, "seconds to complete...\n\n\n")
    
    
def FilterData(VoltageValue, Order, cutoff, SampleRate):
   #Credit for this part of the filter code goes to Warren Weckesser
    #Order = 5
    #nyq = 0.5*19200
    nyq = 0.5*SampleRate
    #cutoff = 9500
    normal_cutoff = cutoff/nyq
    b, a = butter(Order, normal_cutoff, btype='low', analog=False)
    NewVoltageValue = lfilter(b, a, VoltageValue)
    return NewVoltageValue

def BandPassFilter(VoltageValue, Order, LowCutoff, HighCutoff, SampleRate): #Allows only frequencies between low and high through
    nyq = 0.5*SampleRate
    Low = LowCutoff/nyq
    High = HighCutoff/nyq
    b, a = butter(Order, [Low, High], btype='band')
    NewVoltageValue = lfilter(b, a, VoltageValue)
    return NewVoltageValue

def NotchFilter(VoltageValue, Order, LowCutoff, HighCutoff, SampleRate):   #Blocks a specific band in the data
    nyq = 0.5*SampleRate
    Low = LowCutoff/nyq
    High = HighCutoff/nyq
    b, a = butter(Order, [Low, High], btype='bandstop')
    NewVoltageValue = lfilter(b, a, VoltageValue)
    return NewVoltageValue
    

print("Welcome to the Arduino EFM Project Software,\nDeveloped in Partnership Between Tyler Welty and Dr. Phillip Bitzer\nunder funding from the University of Alabama in Huntsville.\n\n")
print("The following functions are available for use: \n")
print("1) ProcessData(FileName and Path, Gain), returns Voltage Value, Time Value, and Raw Data in decimal\n")
print("2) GraphData(TimeValue, VoltageValue), plots ADCval vs TimeVal\n")
print("3) PrewriteSD(FileName and Path, FileLength), creates blank file of desired size\n")
print("4) FilterData(VoltageValue, Order, cutoff, SampleRate), returns a filtered voltage from desired order and cutoff freq.\n")
print("\nFor further information on how to use this software or how the functions work,\nplease consult the readme provided with this software.\n")




