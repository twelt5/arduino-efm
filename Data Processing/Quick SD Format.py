# Arduino EFM SD Format Writer
#   By Tyler Welty under funding from the University of Alabama in Huntsville
# 6/21/2017
"""The Arduino is able to write to the SD card much faster once the card has already
been written to, as the SD already has a file structure in place and the system
can simply focus on rewriting that structure instead of creating it, which is costly
in regards to time when operating on the microsecond scale. The purpose of this code
is to hopefully create that file structure very quickly so that the Arduino can
focus on sampling and not have to use another Arduino code to do so."""

#Header Files
import sys
import time

#Start Time to See how long this takes
StartTime = time.time()

#Open a binary file, same name as will be on Arduino, in write mode
fBin = open("FiveMinute38500sps.bin", 'wb')
fBin.seek(0)

#Set Length of File - Must be a multiple of 512 bytes
i = 23100416   #75 kB

#Create an Array of bytes of 0 value the size of i
BlankData = []

#Write that data to a file
for sample in range(i):
    BlankData.append(bytes(0))
    fBin.write(BlankData[sample])

#Close file, then transfer to SD Card
fBin.close()

#Get End Time and print
print ("Total SD Format Time for ", i, " is: ", time.time()-StartTime)
